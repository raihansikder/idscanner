<section class="sidebar">                  
  <!-- Sidebar user panel -->
  <div class="user-panel">
    
    <div class="pull-left info">
      <p>Hello, Jane</p>

      
    </div>
  </div>
  
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="active">
      <a href="dashboard.php">
        <i class="fa fa-mobile"></i> <span>Dashboard</span>
      </a>
    </li>
    <li>
      <a href="input-barcode.php">
        <i class="fa fa-bar-chart-o"></i> <span>Barcode</span>
      </a>
    </li>
    <li>
      <a href="input-id-scan.php">
        <i class="fa fa-camera-retro"></i> <span>ID Scan</span>
      </a>
    </li>
    <li>
      <a href="input-form.php">
        <i class="fa fa-edit"></i> <span>Form input</span>
      </a>
    </li>
    <li>
      <a href="report-home.php">
        <i class="ion ion-pie-graph"></i> <span>Reports</span>
      </a>
    </li>
    
  </ul>
</section>