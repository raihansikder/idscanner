
<a href="index.php" class="logo">
  <!-- Add the class icon to your logo image or logo icon to add the margining -->
  <i class="fa fa-camera-retro"></i>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </a>
  <div class="navbar-right">
    <ul class="nav navbar-nav">
      <!-- Messages: style can be found in dropdown.less-->
      <li class="dropdown messages-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-envelope"></i>
          <span class="label label-success">2</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have 2 messages</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li><!-- start message -->
                <a href="#">
                  <div class="pull-left">
                    <img src="img/avatar3.png" class="img-circle" alt="User Image"/>
                  </div>
                  <h4>
                    Support Team
                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                  </h4>
                  <p>You are scheduled for new mobile device training on 03/14/2015. Please confirm by return.</p>
                </a>
              </li><!-- end message -->
              <li>
                <a href="#">
                  <div class="pull-left">
                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                  </div>
                  <h4>
                    Support Team
                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                  </h4>
                  <p>Please read the new policy procedures and confirm that you have read </p>
                </a>
              </li>              
            </ul>
          </li>
          <li class="footer"><a href="#">See All Messages</a></li>
        </ul>
      </li>
      <!-- Notifications: style can be found in dropdown.less -->
      <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-warning"></i>
          <span class="label label-warning">3</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have 3 notifications</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li>
                <a href="#">
                  <i class="ion ion-ios7-people info"></i> Heightened awareness for the next 7 days due to increase in threat level.
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-users warning"></i> Stolen White Ford Transit with New York plates suspected in illegal immigration ring.
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="ion ion-ios7-cart success"></i> New format Canadian drivers licence will be in force from 03/15/2015
                </a>
              </li>              
            </ul>
          </li>
          <li class="footer"><a href="#">View all</a></li>
        </ul>
      </li>
      <!-- Tasks: style can be found in dropdown.less -->

      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="glyphicon glyphicon-user"></i>
          <span>Jane Doe <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header bg-light-blue">
            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
            <p>
              Jane Doe - Patrol Officer              
            </p>
          </li>
          <!-- Menu Body -->
          <li class="user-body">
            <div class="col-xs-4 text-center">
              <a href="#">Followers</a>
            </div>
            <div class="col-xs-4 text-center">
              <a href="#">Sales</a>
            </div>
            <div class="col-xs-4 text-center">
              <a href="#">Friends</a>
            </div>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
              <a href="#" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>