<!DOCTYPE html>
<html>
  <head>
    <title>IDSCANNER</title>
    <?php include('./common/head.php') ?>
    <style>
      .small-box h3 {font-size: 18px;}
    </style>
  </head>
  <body class="skin-blue">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
      <?php include('./common/body.header.php'); ?>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include('./common/body.siderbar.left.php'); ?>                
        <!-- /.sidebar -->
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            SELECT SCAN METHOD
            <small>Click below</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-6 col-xs-12">
              <a class="" href="input-barcode.php">
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3>
                        BARCODE
                      </h3>
                      <p>
                        Scan barcode or QR
                      </p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <span class="small-box-footer">
                      More info <i class="fa fa-arrow-circle-right"></i>
                    </span>
                  </div>
                </div><!-- ./col -->
              </a>
              <a class="" href="input-id-scan.php">
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3>
                        ID SCAN
                      </h3>
                      <p>
                        Scan driving license
                      </p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-camera-retro"></i>
                    </div>
                    <span class="small-box-footer">
                      More info <i class="fa fa-arrow-circle-right"></i>
                    </span>
                  </div>
                </div><!-- ./col -->
              </a>
              <a class="" href="input-form.php">
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>
                        FORM
                      </h3>
                      <p>
                        Manual data checking
                      </p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-edit"></i>
                    </div>
                    <span class="small-box-footer">
                      More info <i class="fa fa-arrow-circle-right"></i>
                    </span>
                  </div>
                </div><!-- ./col -->
              </a>
              <a class="" href="report-home.php">
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3>
                        REPORTS
                      </h3>
                      <p>
                        Scan summary
                      </p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-pie-graph"></i>
                    </div>
                    <span class="small-box-footer">
                      More info <i class="fa fa-arrow-circle-right"></i>
                    </span>
                  </div>
                </div><!-- ./col -->
              </a>
            </div>
          </div><!-- /.row -->

          <!-- Main row -->
          <div class="row">

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

    <?php include('./common/js.php'); ?>

  </body>
</html>
