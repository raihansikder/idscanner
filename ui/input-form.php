<!DOCTYPE html>
<html>
  <head>
    <title>IDSCANNER</title>
    <?php include('./common/head.php') ?>
    <style>
      .small-box h3 {font-size: 18px;}
    </style>
  </head>
  <body class="skin-blue">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
      <?php include('./common/body.header.php'); ?>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include('./common/body.siderbar.left.php'); ?>                
        <!-- /.sidebar -->
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            FORM SUBMIT TO CHECK DATA
            <small>Click below</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div  class="col-lg-6 col-xs-12"> 


              <div class="form-group">
                <!--                  <label for="exampleInputEmail1">Email address</label>-->
                <input class="form-control" id="exampleInputEmail1" placeholder="Licence number" type="text">
              </div>
              <div class="form-group">
                <!--                  <label for="exampleInputEmail1">Email address</label>-->
                <input class="form-control" id="exampleInputEmail1" placeholder="Phone" type="text">
              </div>
              <div class="form-group">
                <!--                  <label for="exampleInputEmail1">Email address</label>-->
                <input class="form-control" id="exampleInputEmail1" placeholder="Name" type="text">
              </div>
              <div class="form-group">
                <!--                  <label for="exampleInputEmail1">Email address</label>-->
                <input class="form-control" id="exampleInputEmail1" placeholder="Date of Birth" type="text">
              </div>
              <div class="btn-group">
                <button id="startScanIcon" class="form-group btn btn-primary flat btn-lg">Validate</button>
                <button class="form-group btn bg-gray flat btn-lg">Reset</button>
              </div>



            </div>
            <div class="clearfix"></div>            
          </div><!-- /.row -->
          <div class="row">
            <div id="scanResult" class="col-lg-6 col-xs-12 hidden">
              <div  class="col-lg-12 col-xs-12 ">
                <table class="table table-condensed table-responsive">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th>Scan</th>
                      <th>Verify</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Licence Number</td>
                      <td>JGHIRK*&^DHJDHG865887</td>
                      <td><i class="fa fa-check"></i></td>
                    </tr>
                    <tr>
                      <td>Name</td>
                      <td>Jimmy Conor</td>
                      <td><i class="fa fa-check"></i></td>
                    </tr>
                    <tr>
                      <td>Date of Birth</td>
                      <td>23-05-199=84</td>
                      <td><i class="fa fa-check"></i></td>
                    </tr>
                    <tr>
                     <td>Gender</td>
                      <td>Male</td>
                      <td><i class="fa fa-check"></i></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div  class="col-lg-12 col-xs-12 ">
                <h4>3rd Party Verification Services status</h4>
                <a class="btn btn-social btn-flat bg-green">
                  <i class="fa fa-check"></i> US Federal API
                </a>
                <a class="btn btn-social btn-flat bg-green">
                  <i class="fa fa-check"></i> PSG Security
                </a>
                <a class="btn btn-social btn-flat bg-red">
                  <i class="glyphicon glyphicon-exclamation-sign"></i> EU
                </a>
              </div>
            </div>            
          </div>

          <!-- Main row -->


          <div class="row">

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

    <?php include('./common/js.php'); ?>
    <script type="text/javascript">
      $('#startScanIcon').click(function() {
        //alert('test');
        $('#scanProgress').removeClass('hidden');
        $('#scanResult').removeClass('hidden');
      });

    </script>
  </body>
</html>
